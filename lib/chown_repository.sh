#!/bin/bash

function err {
  echo "$@" 1>&2
}

err `id`
if [ "$(id -u)" != "0" ]; then
  err "This script must be run as root"
  exec sudo -n "$0" "$*"
fi

USER=$1
REPO=$2

err $REPO

if [ ! -d "$REPO" ]; then
  err "chown"
  chown -R $USER:$USER "$REPO"

  err "chmod"
  chmod g+rX "$REPO"
fi

err "done"

exit 0