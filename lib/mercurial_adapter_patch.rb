module Redmine
  module Scm
    module Adapters
      module MercurialAdapterPatch
        def self.included(base) # :nodoc:
          base.send(:include, InstanceMethods)
        end

        module InstanceMethods

          def create_repos
            Dir.mkdir url unless Dir.exists? url
            hg 'init' do |io|
              io.read
            end
            File.open File.expand_path('.hg/hgrc', url), 'w' do |f|
              f.puts '[hooks]'
              f.puts "changegroup = wget -b -q -O /dev/null "\
                       "'#{Setting.protocol}://#{Setting.host_name}/sys/"\
                       "fetch_changesets?key=#{Setting.sys_api_key}"\
                       "&id=#{Repository.find_by_url(url).project.id}'"
            end
            Dir.mkdir dir unless Dir.exists? (dir = File.expand_path('/.hg/largefiles', url))
          end

          def update_repos
          end

          def destroy_repos
          end

        end
      end

      MercurialAdapter.send(:include, MercurialAdapterPatch)
    end
  end
end