module Redmine
  module Scm
    module Adapters
      module GitAdapterPatch
        def self.included(base) # :nodoc:
          base.send(:include, InstanceMethods)
        end

        module InstanceMethods

          def create_repos
            Dir.mkdir url unless Dir.exists? url
            git_cmd ['init', '--bare'] do |io|
              io.read
            end
            Dir.mkdir dir unless Dir.exists? (dir = File.expand_path('hooks', url))
            File.open File.expand_path('hooks/post-receive', url), 'w' do |f| 
              f.puts '#/bin/sh'
              f.puts "wget -b -q -O /dev/null "\
                       "'#{Setting.protocol}://#{Setting.host_name}/sys/"\
                       "fetch_changesets?key=#{Setting.sys_api_key}"\
                       "&id=#{Repository.find_by_url(url).project.id}'"
              f.chmod 0755
            end
          end

          def update_repos
          end

          def destroy_repos
          end

        end
      end

      GitAdapter.send(:include, GitAdapterPatch)
    end
  end
end